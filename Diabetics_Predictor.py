import pandas as pd
from pandas.tools.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_val_score
import numpy as np
import Tkinter as tk
from elm import ClassificationELM

number_of_times_pregnant = 0
plasma_glucose_concentration = 0
diastolic_blood_pressure = 0
triceps_skin_fold_thickness = 0
two_Hour_serum_insulin = 0
bmi = 0
diabetes_pedigree_function = 0
age = 0

col = ['Nil']

df = pd.read_csv("pima-indians-diabetes.csv")
data = df.ix[:, :8]
# print('Success')

print(df.groupby('class').size())

# box and whisker plots
df.plot(kind='box', subplots=True, layout=(2, 5), sharex=False, sharey=False)
plt.show()

# histograms
df.hist()
plt.show()

# scatter plot matrix
scatter_matrix(df)
plt.show()

# Test options and evaluation metric
seed = 7
scoring = 'accuracy'

'''
This method creates and trains a neural network model
'''


def create_model():
    # create model
    model = Sequential()
    model.add(Dense(12, input_dim=8, init='uniform', activation='relu'))
    model.add(Dense(8, init='uniform', activation='relu'))
    model.add(Dense(1, init='uniform', activation='sigmoid'))
    # Compile model
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model


# Split-out validation dataset
data = df.ix[:, :8]
array = df.values
X = array[:, 0:8]
Y = array[:, 8]
validation_size = 0.20
X_train, X_validation, Y_train, Y_validation = model_selection.train_test_split(X, Y, test_size=validation_size,
                                                                                random_state=seed)

# fix random seed for reproducibility
np.random.seed(seed)

# neuron dictionary for elm
neuron_dict = {'linear': 4, 'sigmoid': 4}

models = [('LR', LogisticRegression()), ('LDA', LinearDiscriminantAnalysis()), ('KNN', KNeighborsClassifier()),
          ('CART', DecisionTreeClassifier()), ('NB', GaussianNB()), ('SVM', SVC()),
          ('RandomForest', RandomForestClassifier()),
          ('Neural network', KerasClassifier(build_fn=create_model, nb_epoch=150, batch_size=10, verbose=0)),
          ('ELM', ClassificationELM(neuron_dictionary=neuron_dict))]

# # Create Model For Neural Network(NN)
# model = KerasClassifier(build_fn=create_model, nb_epoch=150, batch_size=10, verbose=0)
# models.append(("Neural network", model))



# evaluate each model in turn
results = []
names = []
means = []

#try:
for name, model in models:
    if name == 'Neural network':
        kfold = StratifiedKFold(n_splits=10, shuffle=True, random_state=seed)
    else:
        kfold = model_selection.KFold(n_splits=10, random_state=seed)
    cv_results = cross_val_score(model, X_train, Y_train, cv=kfold, scoring=scoring)
    results.append(cv_results)
    names.append(name)
    msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
    means.append(cv_results.mean())
    print(msg)

#except  ValueError as e:
#    print("Exception",name)

# Compare Algorithms
fig = plt.figure()
fig.suptitle('Algorithm Comparison')
ax = fig.add_subplot(111)
plt.boxplot(results)
ax.set_xticklabels(names)
plt.show()

acc = models[means.index(max(means))]
accurate = acc[1]

# Make predictions on validation dataset
print acc[0]
algm = accurate
algm.fit(X_train, Y_train)
predictions = algm.predict(X_validation)
print("acc", accuracy_score(Y_validation, predictions))
print("conf", confusion_matrix(Y_validation, predictions))
print(classification_report(Y_validation, predictions))

# Logic for GUI

root = tk.Tk()

tk.Label(root, text='number_of_times_pregnant').grid(row=1, column=0)
e1 = tk.Entry(root)
e1.grid(row=1, column=2)
tk.Label(root, text='plasma_glucose_concentration').grid(row=2, column=0)
e2 = tk.Entry(root)
e2.grid(row=2, column=2)
tk.Label(root, text='diastolic_blood_pressure ').grid(row=3, column=0)
e3 = tk.Entry(root)
e3.grid(row=3, column=2)
tk.Label(root, text='triceps_skin_fold_thickness').grid(row=4, column=0)
e4 = tk.Entry(root)
e4.grid(row=4, column=2)
tk.Label(root, text='two_Hour_serum_insulin').grid(row=5, column=0)
e5 = tk.Entry(root)
e5.grid(row=5, column=2)
tk.Label(root, text='bmi').grid(row=6, column=0)
e6 = tk.Entry(root)
e6.grid(row=6, column=2)
tk.Label(root, text='diabetes_pedigree_function').grid(row=7, column=0)
e7 = tk.Entry(root)
e7.grid(row=7, column=2)
tk.Label(root, text='age').grid(row=8, column=0)
e8 = tk.Entry(root)
e8.grid(row=8, column=2)


def callback():
    # Defining variables as Global
    global number_of_times_pregnant
    global plasma_glucose_concentration
    global diastolic_blood_pressure
    global triceps_skin_fold_thickness
    global two_Hour_serum_insulin
    global bmi
    global diabetes_pedigree_function
    global age

    # Assigning Values
    number_of_times_pregnant = float(e1.get())
    plasma_glucose_concentration = float(e2.get())
    diastolic_blood_pressure = float(e3.get())
    triceps_skin_fold_thickness = float(e4.get())
    two_Hour_serum_insulin = float(e5.get())
    bmi = float(e6.get())
    diabetes_pedigree_function = float(e7.get())
    age = float(e8.get())
    new = algm.predict(np.array([number_of_times_pregnant, plasma_glucose_concentration,
                                 diastolic_blood_pressure, triceps_skin_fold_thickness,
                                 two_Hour_serum_insulin, bmi, diabetes_pedigree_function, age]))

    if new == 1:
        tk.Label(root, text='The Person has the risk of diabetics').grid(row=10)
    if new == 0:
        tk.Label(root, text='The Person is free from  diabetics').grid(row=10)


b = tk.Button(root, text="Predict", width=10, command=callback).grid(row=9, column=1)

tk.mainloop()
